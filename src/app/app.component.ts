import { Component, OnInit } from '@angular/core';
import * as tree from 'tree-model';
import * as d3 from 'd3';
import * as _ from 'underscore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  public root: any;
  public width: number = 1500;
  public height: number = 1500;
  public innerWidth: any;
  public innerHeight: any;
  public numberOfCampusDB: number;
  public calculateMidPoint: number;
  public beginingPoints: any;
  public firstLevelEndPoints: Array<any> = [];
  public secondLevelEndPoints: Array<any> = [];
  public widgetColor;
 
  campusDashboardsHierarchy = {
    "id": 0,
    "name": "Root",
    "children": [
      {
        "id": 1,
        "name": "MVA Switchgear Dashboard",
        "color":"#33cc00",
        "children": [
          {
            "id": 11,
            "name": "LVA Switchgear Dashboard 1",
            "color":"#33cc00",
            "children": [
             {
                "id": 111,
                "name": "Switchboard 1 Dashboard",
                "color": "#e60000",
                "children": []
              },
              {
                "id": 112,
                "name": "Switchboard 2 Dashboard",
                "color": "#33cc00",
                "children": []
              }
            ]
          },
          {
            "id": 12,
            "name": "LVA Switchgear Dashboard 2",
            "color": "#e60000",
            "children": []
          },
          {
            "id": 13,
            "name": "LVA Switchgear Dashboard 3",
            "color": "#e60000",
            "trafoConnected": true,
            "children": [
              {
                "id": 131,
                "name": "Switchboard 1 Dashboard",
                "color":"#33cc00",
                "children": []
              },
              {
                "id": 132,
                "name": "Switchboard 2 Dashboard",
                "color": "#e60000",
                "children": []
              },
              {
                "id": 133,
                "name": "LVMCC 1 Dashboard",
                "color": "#ff6600",
                "children": []
              }
            ]
          },
          {
            "id": 14,
            "name": "LVA Switchgear Dashboard 4",
            "trafoConnected": true,
            "color":"#33cc00",
            "children": []
          },
          {
            "id": 14,
            "name": "LVA Switchgear Dashboard 4",
            "trafoConnected": true,
            "color":"#33cc00",
            "children": []
          },
          {
            "id": 14,
            "name": "LVA Switchgear Dashboard 4",
            "trafoConnected": true,
            "color":"#33cc00",
            "children": []
          }
        ]
      }
    ]
  }
  constructor() {}
 
  ngOnInit() {      
    this.innerWidth = window.innerWidth + 1500; 
    this.innerHeight = window.innerHeight;   
     
    var svg = d3.select("body").append("svg")
              .attr("width", this.innerWidth)
              .attr("height", this.innerHeight)
             
    
    this.numberOfCampusDB = this.campusDashboardsHierarchy.children.length;
    this.calculateMidPoint = this.innerWidth / (this.campusDashboardsHierarchy.children.length + 1);
    this.beginingPoints = {x: this.calculateMidPoint, y: 50, width: 230, height: 140, distBetweenLevels: 150, distForNameX:10, distForNameY: 30, innerRectHeight: 50, bottomLineY: 35};
    var b1 = svg.append("g");  
    
    for(var i = 0; i < this.campusDashboardsHierarchy.children.length; i++) {
      this.firstLevelEndPoints = [];
      var childCountofSecondLevel = this.campusDashboardsHierarchy.children[i].children.length;

      b1.append("line")
      .attr("x1", (this.beginingPoints.x - (this.beginingPoints.width/2)) + 40)
      .attr("y1", this.beginingPoints.y - 30)
      .attr("x2",(this.beginingPoints.x - (this.beginingPoints.width/2)) + 40)
      .attr("y2", this.beginingPoints.y )
      .attr("stroke-width", 2).attr("stroke", "#4d4d4d")     
      b1.append("line")
      .attr("x1", (this.beginingPoints.x - (this.beginingPoints.width/2)) - 140)
      .attr("y1", this.beginingPoints.y - 30)
      .attr("x2",(this.beginingPoints.x - (this.beginingPoints.width/2)) + 40)
      .attr("y2", this.beginingPoints.y - 30)
      .attr("stroke-width", 2).attr("stroke", "#4d4d4d")
      b1.append("image")
        .attr("xlink:href", "https://static.thenounproject.com/png/4747-200.png")
        .attr("x", (this.beginingPoints.x - (this.beginingPoints.width/2)) - 180)
        .attr("y", this.beginingPoints.y - 48)
        .attr("width", 60).attr("height", 50);

      b1.append("line")
      .attr("x1", (this.beginingPoints.x + (this.beginingPoints.width/2)) - 40)
      .attr("y1", this.beginingPoints.y - 30)
      .attr("x2",(this.beginingPoints.x + (this.beginingPoints.width/2)) - 40)
      .attr("y2", this.beginingPoints.y )
      .attr("stroke-width", 2).attr("stroke", "#4d4d4d")
      b1.append("line")
      .attr("x1", (this.beginingPoints.x + (this.beginingPoints.width/2)) - 40)
      .attr("y1", this.beginingPoints.y - 30)
      .attr("x2",(this.beginingPoints.x + (this.beginingPoints.width/2)) + 140)
      .attr("y2", this.beginingPoints.y - 30)
      .attr("stroke-width", 2).attr("stroke", "#4d4d4d")
      b1.append("image")
      .attr("xlink:href", "https://static.thenounproject.com/png/4747-200.png")
      .attr("x", (this.beginingPoints.x + (this.beginingPoints.width/2)) + 100)
      .attr("y", this.beginingPoints.y - 48)
      .attr("width", 60).attr("height", 50);


      b1.append("rect").attr("height", this.beginingPoints.height).attr("width", this.beginingPoints.width).attr("x", (this.beginingPoints.x - (this.beginingPoints.width/2))).attr("y", this.beginingPoints.y)
          .attr("stroke", "#686868").attr("stroke-width", 1).attr("fill", "none");

      b1.append("rect").attr("height", this.beginingPoints.innerRectHeight).attr("width", this.beginingPoints.width).attr("x", (this.beginingPoints.x - (this.beginingPoints.width/2))).attr("y", this.beginingPoints.y)
          .attr("stroke", this.campusDashboardsHierarchy.children[i].color).attr("stroke-width", 1).attr("fill", this.campusDashboardsHierarchy.children[i].color);
    
      b1.append("text").attr("x",(this.beginingPoints.x - (this.beginingPoints.width/2)) + this.beginingPoints.distForNameX)
      .attr("y",this.beginingPoints.y + this.beginingPoints.distForNameY).attr("fill", "white").attr("font-size", "16px").text(this.campusDashboardsHierarchy.children[i].name);
     
      b1.append("line")
      .attr("x1", (this.beginingPoints.x - (this.beginingPoints.width/2)))
      .attr("y1", this.beginingPoints.y+ this.beginingPoints.height - this.beginingPoints.bottomLineY)
      .attr("x2",(this.beginingPoints.x + (this.beginingPoints.width/2)))
      .attr("y2", this.beginingPoints.y + this.beginingPoints.height - this.beginingPoints.bottomLineY)
      .attr("stroke-width", 0.5).attr("stroke", "#4d4d4d") 
      
      b1.append("text").attr("x",(this.beginingPoints.x - (this.beginingPoints.width/2))+10)
      .attr("y",this.beginingPoints.y+ this.beginingPoints.height - 12).attr("fill", "#808080").attr("font-size", "15px").text("View Dashboard");
       
      //Vertical Line of First Box
      b1.append("line")
      .attr("x1", this.beginingPoints.x)
      .attr("y1", this.beginingPoints.y+ this.beginingPoints.height)
      .attr("x2",this.beginingPoints.x)
      .attr("y2", this.beginingPoints.y + this.beginingPoints.height+ (this.beginingPoints.distBetweenLevels/2))
      .attr("stroke-width", 2).attr("stroke", "black")

      this.firstLevelEndPoints.push({
        "x1": (this.beginingPoints.x - (this.beginingPoints.width/2)),
        "y1": this.beginingPoints.y,
        //"x2": (this.beginingPoints.x - (this.beginingPoints.width/2)),
        "x2": (childCountofSecondLevel === 2) ? this.innerWidth/2.5 : this.innerWidth/childCountofSecondLevel,
        "y2": this.beginingPoints.y + this.beginingPoints.height + this.beginingPoints.distBetweenLevels,
        "width": this.beginingPoints.width,
        "height": this.beginingPoints.height,
        "innerRectHeight": this.beginingPoints.innerRectHeight,
        "bottomLineY": this.beginingPoints.bottomLineY
      })
     
      this.drawSecondLevel(b1, this.firstLevelEndPoints, this.campusDashboardsHierarchy.children[i]);

    }   
   
  }

   drawSecondLevel = function (b1, firstLevelEndPoints, secondLevelChilds) {
    for(var i = 0; i < secondLevelChilds.children.length; i++) {
      this.widgetColor = '';
      this.secondLevelEndPoints = [];
      console.log("**2*",this.widgetColor )
      b1.append("line")
      .attr("x1", (firstLevelEndPoints[0].x2 + (firstLevelEndPoints[0].width/2)))
      .attr("y1", firstLevelEndPoints[0].y2 - (this.beginingPoints.distBetweenLevels/2))
      .attr("x2",(firstLevelEndPoints[0].x2 + (firstLevelEndPoints[0].width/2)))
      .attr("y2", firstLevelEndPoints[0].y2)
      .attr("stroke-width", 2).attr("stroke", "black")
    
      if (secondLevelChilds.children[i].trafoConnected){
        b1.append("image")
        .attr("xlink:href", "https://image.flaticon.com/icons/png/512/114/114761.png")
        .attr("x", (firstLevelEndPoints[0].x2 + (firstLevelEndPoints[0].width/2)) - 18)
        .attr("y", firstLevelEndPoints[0].y2 - (this.beginingPoints.distBetweenLevels/2) + 15)
        .attr("width", 35).attr("height", 35);
      }


      b1.append("rect").attr("height", firstLevelEndPoints[0].height).attr("width", firstLevelEndPoints[0].width).attr("x", firstLevelEndPoints[0].x2).attr("y", firstLevelEndPoints[0].y2)
      .attr("stroke", "#686868").attr("stroke-width", 1).attr("fill", "none");

      b1.append("rect").attr("height", firstLevelEndPoints[0].innerRectHeight).attr("width", firstLevelEndPoints[0].width).attr("x", firstLevelEndPoints[0].x2).attr("y", firstLevelEndPoints[0].y2)
      .attr("stroke", secondLevelChilds.children[i].color).attr("stroke-width", 1).attr("fill", secondLevelChilds.children[i].color);
           
      b1.append("text").attr("x",firstLevelEndPoints[0].x2 + this.beginingPoints.distForNameX)
      .attr("y",firstLevelEndPoints[0].y2 + this.beginingPoints.distForNameY).attr("fill", "white").attr("font-size", "16px").text(secondLevelChilds.children[i].name);
   
      b1.append("line")
      .attr("x1", firstLevelEndPoints[0].x2)
      .attr("y1", firstLevelEndPoints[0].y2 + firstLevelEndPoints[0].height - firstLevelEndPoints[0].bottomLineY)
      .attr("x2",firstLevelEndPoints[0].x2 + firstLevelEndPoints[0].width)
      .attr("y2", firstLevelEndPoints[0].y2 +firstLevelEndPoints[0].height - firstLevelEndPoints[0].bottomLineY)
      .attr("stroke-width", 1).attr("stroke", "#4d4d4d") 
      
      b1.append("text").attr("x",firstLevelEndPoints[0].x2 + 10)
      .attr("y",firstLevelEndPoints[0].y2 + firstLevelEndPoints[0].height - 12).attr("fill", "#808080").attr("font-size", "15px").text("View Dashboard");
          

      this.secondLevelEndPoints.push({
        "x1": firstLevelEndPoints[0].x2,
        "y1": firstLevelEndPoints[0].y2,
        "x2": (secondLevelChilds.children[i].children.length === 1) ? firstLevelEndPoints[0].x2 : firstLevelEndPoints[0].x2 - 20,
        "y2": firstLevelEndPoints[0].y2 + firstLevelEndPoints[0].height + this.beginingPoints.distBetweenLevels,
        "width": firstLevelEndPoints[0].width,
        "height": firstLevelEndPoints[0].height,
        "innerRectHeight": firstLevelEndPoints[0].innerRectHeight,
        "bottomLineY": firstLevelEndPoints[0].bottomLineY
      })
     
      if (secondLevelChilds.children[i].children.length ) {
        b1.append("line")
        .attr("x1", (firstLevelEndPoints[0].x2 + (firstLevelEndPoints[0].width/2)))
        .attr("y1", firstLevelEndPoints[0].y2 + firstLevelEndPoints[0].height)
        .attr("x2",(firstLevelEndPoints[0].x2 + (firstLevelEndPoints[0].width/2)))
        .attr("y2", firstLevelEndPoints[0].y2 + firstLevelEndPoints[0].height + (this.beginingPoints.distBetweenLevels/2))
        .attr("stroke-width", 2).attr("stroke", "black")
        this.drawThirdLevel (b1, this.secondLevelEndPoints, secondLevelChilds.children[i])
      } else {        
        this.firstLevelEndPoints[0].x2 = firstLevelEndPoints[0].x2 + firstLevelEndPoints[0].width + 20; 
        if (i !== (secondLevelChilds.children.length-1)) {
        b1.append("line")
        .attr("x1", firstLevelEndPoints[0].x2 - (firstLevelEndPoints[0].width/2) - 20)
        .attr("y1", firstLevelEndPoints[0].y2 - (this.beginingPoints.distBetweenLevels/2))
        .attr("x2",firstLevelEndPoints[0].x2 + (firstLevelEndPoints[0].width/2))
        .attr("y2", firstLevelEndPoints[0].y2 - (this.beginingPoints.distBetweenLevels/2))    
        .attr("stroke-width", 2).attr("stroke", "black") }
      }
    }
    
  }

  drawThirdLevel = function(b1, secondLevelEndPoints, thirdLevelChilds) {   
     for(var i = 0; i < thirdLevelChilds.children.length; i++) {
      console.log("**3*", thirdLevelChilds)
       console.log("HHH", thirdLevelChilds)
       b1.append("rect").attr("height", secondLevelEndPoints[0].height).attr("width", secondLevelEndPoints[0].width).attr("x", secondLevelEndPoints[0].x2).attr("y", secondLevelEndPoints[0].y2)
       .attr("stroke", "#686868").attr("stroke-width", 1).attr("fill", "none");     
     
       b1.append("rect").attr("height", secondLevelEndPoints[0].innerRectHeight).attr("width", secondLevelEndPoints[0].width).attr("x", secondLevelEndPoints[0].x2).attr("y", secondLevelEndPoints[0].y2)
       .attr("stroke", thirdLevelChilds.children[i].color).attr("stroke-width", 1).attr("fill", thirdLevelChilds.children[i].color); 
       
       b1.append("text").attr("x", secondLevelEndPoints[0].x2 + this.beginingPoints.distForNameX)
       .attr("y",secondLevelEndPoints[0].y2 + this.beginingPoints.distForNameY).attr("fill", "white").attr("font-size", "16px").text(thirdLevelChilds.children[i].name);
      
       b1.append("line")
      .attr("x1", secondLevelEndPoints[0].x2)
      .attr("y1", secondLevelEndPoints[0].y2 + secondLevelEndPoints[0].height - secondLevelEndPoints[0].bottomLineY)
      .attr("x2",secondLevelEndPoints[0].x2 + secondLevelEndPoints[0].width)
      .attr("y2", secondLevelEndPoints[0].y2 +secondLevelEndPoints[0].height - secondLevelEndPoints[0].bottomLineY)
      .attr("stroke-width", 1).attr("stroke", "#4d4d4d") 
      
      b1.append("text").attr("x",secondLevelEndPoints[0].x2 + 10)
      .attr("y",secondLevelEndPoints[0].y2 + secondLevelEndPoints[0].height - 12).attr("fill", "#808080").attr("font-size", "15px").text("View Dashboard");
          
      if (i !== (thirdLevelChilds.children.length-1)) {
       b1.append("line")
        .attr("x1", secondLevelEndPoints[0].x2 + (secondLevelEndPoints[0].width/2))
        .attr("y1", secondLevelEndPoints[0].y2 - (this.beginingPoints.distBetweenLevels/2))
        .attr("x2",secondLevelEndPoints[0].x2 + (secondLevelEndPoints[0].width/2) + secondLevelEndPoints[0].width + 20)
        .attr("y2", secondLevelEndPoints[0].y2 - (this.beginingPoints.distBetweenLevels/2))
        .attr("stroke-width", 2).attr("stroke", "black") }

       b1.append("line")
        .attr("x1", secondLevelEndPoints[0].x2 + (secondLevelEndPoints[0].width/2))
        .attr("y1", secondLevelEndPoints[0].y2 - (this.beginingPoints.distBetweenLevels/2))
        .attr("x2",secondLevelEndPoints[0].x2 + (secondLevelEndPoints[0].width/2))
        .attr("y2", secondLevelEndPoints[0].y2)
        .attr("stroke-width", 2).attr("stroke", "black")
       this.secondLevelEndPoints[0].x2 = secondLevelEndPoints[0].x2 + secondLevelEndPoints[0].width + 20;
       this.secondLevelEndPoints[0].y2 = secondLevelEndPoints[0].y2;
             
    }
 

    b1.append("line")
    .attr("x1", (this.firstLevelEndPoints[0].x2 + (this.firstLevelEndPoints[0].width/2)))
    .attr("y1", this.firstLevelEndPoints[0].y2 - (this.beginingPoints.distBetweenLevels/2))
    .attr("x2",(secondLevelEndPoints[0].x2 + 20)+(secondLevelEndPoints[0].width/2))
    .attr("y2", this.firstLevelEndPoints[0].y2 - (this.beginingPoints.distBetweenLevels/2))
    .attr("stroke-width", 2).attr("stroke", "black")
    
    this.firstLevelEndPoints[0].x1 = (this.beginingPoints.x - (this.beginingPoints.width/2));
    this.firstLevelEndPoints[0].y1 = this.beginingPoints.y;
    this.firstLevelEndPoints[0].x2 = secondLevelEndPoints[0].x2 + 20;
    this.firstLevelEndPoints[0].y2 = secondLevelEndPoints[0].y1;
    this.firstLevelEndPoints[0].width = secondLevelEndPoints[0].width;
    this.firstLevelEndPoints[0].height = secondLevelEndPoints[0].height;
    this.firstLevelEndPoints[0].innerRectHeight = secondLevelEndPoints[0].innerRectHeight
  
  }
 
}

